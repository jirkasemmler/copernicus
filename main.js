var activeCities = [];
var activeSeries = ['salary', 'vegetation', 'big_mac'];
var map = null;

var coords = {
    'Bombai': {'lat': 19.12, 'lng': 72.85},
    'NewYork': {'lat': 40.59, 'lng': -73.98},
    'Brno': {'lat': 49.15, 'lng': 16.69},
    'Zurich': {'lat': 47.38, 'lng': 8.57},
    'Beijing': {'lat': 39.93, 'lng': 116.28},
    'Nairobi': {'lat': -1.3, 'lng': 36.75},
    'Helsinki': {'lat': 60.17, 'lng': 24.95},
    'Barcelona': {'lat': 41.38, 'lng': 2.17},
    'Manilla': {'lat': 14.55, 'lng': 120.98}
};
var maxtemp = {
    'Bombai': [31.63, 31.73, 32.48, 32.62, 32.91, 32.26, 31.45, 31.33, 31.47, 32.73, 32.91, 32.2],
    'NewYork': [4.44, 5.84, 9.92, 15.65, 20.82, 25.5, 28.14, 27.52, 23.89, 18.34, 12.65, 6.54],
    'Brno': [8.75, 7.2, 10.88, 14.81, 18.8, 20.97, 22.86, 23.27, 19.27, 14.31, 10.08, 9.27],
    'Zurich': [3.02, 5.15, 10.03, 14.34, 19.12, 22.37, 24.37, 23.74, 19.86, 13.92, 7.37, 3.62],
    'Beijing': [1.65, 4.78, 11.53, 20.19, 26.56, 30.39, 31.14, 29.87, 25.98, 19.15, 10.06, 3.28],
    'Nairobi': [25.19, 25.83, 25.3, 24.85, 23.3, 22.38, 21.26, 22.32, 24.43, 25.05, 23.07, 23.17],
    'Helsinki': [-1.64, -2.36, 1.34, 7.03, 13.9, 18.64, 21.03, 19.61, 14.53, 8.92, 3.87, 0.49],
    'Barcelona': [14.87, 14.81, 17.13, 19.5, 22.5, 26.65, 28.9, 29.22, 26.22, 22.88, 18.41, 15.46],
    'Manilla': [30.49, 30.88, 32.09, 33.22, 33.31, 32.47, 31.54, 31.42, 31.37, 31.57, 31.36, 30.66]
};
var mintemp = {
    'Bombai': [19.66, 20.15, 21.57, 22.86, 24.1, 22.86, 22.65, 22.93, 22.76, 22.4, 21.6, 20.58],
    'NewYork': [-2.58, -1.76, 1.76, 6.71, 11.76, 16.69, 19.69, 19.28, 15.62, 10.06, 5.33, -0.15],
    'Brno': [7.45, 5.57, 5.92, 6.62, 7.96, 9.44, 10.57, 9.55, 8.49, 7.24, 7.41, 7.6],
    'Zurich': [-2.38, -1.96, 1.11, 4.26, 8.25, 11.53, 13.39, 13.01, 10.18, 6.25, 1.75, -1.22],
    'Beijing': [-8.65, -5.95, 0.18, 7.78, 13.82, 18.81, 22.04, 20.85, 14.85, 7.78, -0.07, -6.25],
    'Nairobi': [12.98, 12.84, 14.5, 15.19, 13.84, 13.18, 12.05, 12.74, 12.99, 13.59, 13.87, 13.89],
    'Helsinki': [-7.24, -7.84, -4.76, 0.46, 6.09, 11.02, 13.94, 12.98, 8.69, 4.05, -0.42, -4.38],
    'Barcelona': [8.65, 8.27, 10.25, 12.91, 15.78, 20.07, 22.92, 23.33, 20.33, 16.9, 12.06, 9.27],
    'Manilla': [24.03, 24.12, 24.54, 25.05, 24.82, 24.73, 24.67, 24.63, 24.68, 24.67, 24.63, 24.49]
};


var vegetation = {
    'Brno': [0.33],
    'Barcelona': [0.24],
    'Helsinki': [0.27],
    'Beijing': [0.31],
    'Manilla': [0.16],
    'Zurich': [0.39],
    'NewYork': [0.12],
    'Bombai': [0.24],
    'Nairobi': [0.21]
};

var abs_vegetation = {
    'Brno': [0.33],
    'Barcelona': [0.24],
    'Helsinki': [0.27],
    'Beijing': [0.31],
    'Manilla': [0.16],
    'Zurich': [0.39],
    'NewYork': [0.12],
    'Bombai': [0.24],
    'Nairobi': [0.21]
};

var humidity = {
    'Brno': [0.4],
    'Barcelona': [0.3],
    'Helsinki': [0.4],
    'Beijing': [0.4],
    'Manilla': [0.4],
    'Zurich': [0.4],
    'NewYork': [0.35],
    'Bombai': [0.5],
    'Nairobi': [0.2]
};


var abs_humidity = {
    'Brno': [0.6],
    'Barcelona': [0.7],
    'Helsinki': [0.6],
    'Beijing': [0.6],
    'Manilla': [0.6],
    'Zurich': [0.6],
    'NewYork': [0.65],
    'Bombai': [0.5],
    'Nairobi': [0.8],
};

var population = {
    'Brno': [380000],
    'Barcelona': [600000],
    'Helsinki': [632000],
    'Beijing': [500000],
    'Manilla': [800000],
    'Zurich': [403000],
    'NewYork': [8600000],
    'Bombai': [18400000],
    'Nairobi': [3100000]
};

var titles = {
    'vegetation': 'Vegetation [Higher better]',
    'salary': 'Salary [Higher better]',
    'big_mac': 'Big max index [Lower better]',
    'dens': 'Population density [Lower better]',
    'humidity': 'Humidity [Lower better]',
    'co': 'CO [Lower better]',
    'ch': 'CH4 [Lower better]',
    'no': 'NO2 [Lower better]',
    'so': 'SO2 [Lower better]',
};

var dens = {
    'Brno': [1700],
    'Barcelona': [16000],
    'Helsinki': [16494],
    'Beijing': [6000],
    'Manilla': [41515],
    'Zurich': [4700],
    'NewYork': [10194],
    'Bombai': [28508],
    'Nairobi': [3950]
};
var abs_dens = {
    'Brno': [0.9591],
    'Barcelona': [0.6146],
    'Helsinki': [0.6027],
    'Beijing': [0.8555],
    'Manilla': [0.0000],
    'Zurich': [0.8868],
    'NewYork': [0.7545],
    'Bombai': [0.3133],
    'Nairobi': [0.9049],
};


var co = {
    'Brno': [100],
    'Barcelona': [90],
    'Helsinki': [110],
    'Beijing': [1200],
    'Manilla': [80],
    'Zurich': [110],
    'NewYork': [1000],
    'Bombai': [60],
    'Nairobi': [70]
};

var abs_co = {
    'Brno': [0.917],
    'Barcelona': [0.925],
    'Helsinki': [0.908],
    'Beijing': [0.0],
    'Manilla': [0.933],
    'Zurich': [0.908],
    'NewYork': [0.167],
    'Bombai': [0.950],
    'Nairobi': [0.942]
};

var ch = {
    'Brno': [1830],
    'Barcelona': [1900],
    'Helsinki': [1790],
    'Beijing': [1970],
    'Manilla': [1850],
    'Zurich': [1840],
    'NewYork': [1820],
    'Bombai': [1850],
    'Nairobi': [1870]
};

var abs_ch = {
    'Brno': [0.425],
    'Barcelona': [0.25],
    'Helsinki': [0.525],
    'Beijing': [0.075],
    'Manilla': [0.375],
    'Zurich': [0.4],
    'NewYork': [0.45],
    'Bombai': [0.375],
    'Nairobi': [0.325]
};

var no = {
    'Brno': [15],
    'Barcelona': [12],
    'Helsinki': [7],
    'Beijing': [70],
    'Manilla': [10],
    'Zurich': [20],
    'NewYork': [35],
    'Bombai': [22],
    'Nairobi': [3]
};

var abs_no = {
    'Brno': [0.85],
    'Barcelona': [0.88],
    'Helsinki': [0.93],
    'Beijing': [0.3],
    'Manilla': [0.9],
    'Zurich': [0.8],
    'NewYork': [0.65],
    'Bombai': [0.78],
    'Nairobi': [0.97]
};

var so = {
    'Brno': [3],
    'Barcelona': [8],
    'Helsinki': [1],
    'Beijing': [90],
    'Manilla': [50],
    'Zurich': [2],
    'NewYork': [13],
    'Bombai': [60],
    'Nairobi': [3]
};

var abs_so = {
    'Brno': [0.97],
    'Barcelona': [0.92],
    'Helsinki': [0.99],
    'Beijing': [0.1],
    'Manilla': [0.5],
    'Zurich': [0.98],
    'NewYork': [0.87],
    'Bombai': [0.4],
    'Nairobi': [0.97]
};

var salary = {
    'Brno': [1863.00],
    'Barcelona': [1740.00],
    'Helsinki': [5523.00],
    'Beijing': [1200.00],
    'Manilla': [125.00],
    'Zurich': [5896.00],
    'NewYork': [4612.00],
    'Bombai': [2845.00],
    'Nairobi': [664.00]
};

var big_mac = {
    'Brno': [3.8],
    'Barcelona': [4.8],
    'Helsinki': [5.6],
    'Beijing': [3.2],
    'Manilla': [2.6],
    'Zurich': [6.8],
    'NewYork': [5.3],
    'Bombai': [2.8],
    'Nairobi': [2.4]
};

var abs_salary = {
    'Brno': [0.316],
    'Barcelona': [0.295],
    'Helsinki': [0.936],
    'Beijing': [0.203],
    'Manilla': [0.021],
    'Zurich': [0.999],
    'NewYork': [0.782],
    'Bombai': [0.482],
    'Nairobi': [0.113],
};
var abs_big_mac = {
    'Brno': [0.441],
    'Barcelona': [0.294],
    'Helsinki': [0.176],
    'Beijing': [0.529],
    'Manilla': [0.618],
    'Zurich': [0.000],
    'NewYork': [0.221],
    'Bombai': [0.588],
    'Nairobi': [0.647],
};


function fce(what) {
    var series = [];

    activeSeries.push(what);

    activeCities.forEach(function (item) {
        series.push({
            name: item,
            data: window[what][item],
        });
    });
    Highcharts.chart(what, {
        chart: {
            type: 'column'
        },
        title: {
            text: titles[what]
        },
        credits: {
            enabled: false
        },
        series: series
    });

    $('#' + what).fadeIn();
}

function total() {
    var series = [];
    activeCities.forEach(function (cityInit) {
        series[cityInit] = [0]
    });

    var outSeries = [];


    activeCities.forEach(function (city) {
        activeSeries.forEach(function (serie) {
            if (!(serie == 'mintemp' || serie == 'maxtemp')) {
                series[city][0] = series[city][0] + window['abs_' + serie][city][0];
            }

        });
        series[city][0] = Math.round((series[city][0] / activeSeries.length) * 100) / 100;
        outSeries.push({
            name: city,
            data: series[city]
        });
    });

    Highcharts.chart('total', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Total'
        },
        credits: {
            enabled: false
        },
        series: outSeries
    });

}

function addCityToList(item) {
    $('.loader').show();
    var text = $(item.target).data('name');
    if (activeCities.includes(text)) {
        $('.loader').hide();

        return;
    }
    console.log('called addc ' + text);
    var city = $('<button>').addClass('btn btn-primary city-btn').html(text).attr('data-name', text).attr('id', 'btn' + text);

    var icon = $('<i>').addClass('fa fa-times remove-button').attr('data-target', text);
    $(city).append(icon);

    $('#buttons-city').prepend(city);
    $(item.target).remove();
    activeCities.push(text);

    initSeries();
    $(".remove-button").click(removeCityFromList);


    placeMarker(coords[text], map, text);
    $('.loader').hide();

}

function removeCityFromList(item) {

    $('.loader').show();


    var target = $(this).data('target');

    if (!activeCities.includes(target)) {
        $('.loader').hide();

        return;
    }

    $('[data-name=' + target + ']').remove();
    var city = $('<a>').addClass('dropdown-item drop-city').html(target).attr('data-name', target);
    // $('#buttons-city').prepend(city);

    $('#dropdownMenu').prepend(city);
    $(".drop-city").click(addCityToList);
    activeCities.splice($.inArray(target, activeCities), 1);

    removeMarker(target);
    initSeries();
    total();

    $('.loader').hide();

}

$(document).ready(function () {
    var cities = ["Bombai", 'NewYork', 'Brno', 'Barcelona', 'Beijing',];

    cities.forEach(function (item) {
        var city = $('<button>').addClass('btn btn-primary city-btn').html(item).attr('id', 'btn' + item).attr('data-name', item);
        // city.data('name', item);
        var icon = $('<i>').addClass('fa fa-times remove-button').attr('data-target', item);
        $(city).append(icon);
        $('#buttons-city').prepend(city);
        placeMarker(coords[item], map, item);
        activeCities.push(item);
    });

    var inactiveCities = ['Manilla', 'Zurich', 'Helsinki', 'Nairobi'];
    inactiveCities.forEach(function (item) {
        // <a class="dropdown-item" href="#">Action</a>
        var city = $('<a>').addClass('dropdown-item drop-city').html(item).attr('data-name', item);

        // $('#buttons-city').prepend(city);

        $('#dropdownMenu').prepend(city);
        // placeMarker(coords[city], map, city);
    });


    $(".drop-city").click(addCityToList);

    $(".remove-button").click(removeCityFromList);

    initSeries();

    $(".btn-int").click(function (item) {
        $(this).toggleClass('btn-primary-active');
    });

    $('.btn-int').on('click', function (item) {
        var fun = $(this).data('int');
        if ($(this).hasClass('btn-primary-active')) {
            if (fun == 'maxtemp' || fun == 'mintemp') {
                initTemp(fun);
            } else {
                window['fce'](fun);
            }
        } else {
            activeSeries.splice($.inArray(fun, activeSeries), 1);
            $('#' + fun).fadeOut();
        }
        var int = $(this).data('int');

        total();
    });
});


function initSeries() {
    activeSeries.forEach(function (item) {
        if (item == 'maxtemp' || item == 'mintemp') {
            initTemp(item);
        }
        fce(item);
    });

    total();
}

function initTemp(what) {
    activeSeries.push(what);

    var series = [];
    activeCities.forEach(function (item) {
        series.push(
            {
                name: item,
                data: window[what][item]
            }
        );
    });
    var title = what == 'maxtemp' ? 'Max temperature' : 'Min temperature';

    Highcharts.chart(what, {
        chart: {
            type: 'column'
        },
        title: {
            text: title
        },
        xAxis: {
            categories: [
                'Jan',
                'Feb',
                'Mar',
                'Apr',
                'May',
                'Jun',
                'Jul',
                'Aug',
                'Sep',
                'Oct',
                'Nov',
                'Dec'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: series
    });

    $('#' + what).fadeIn();
}

var style =
    [
        {
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f5f5f5"
                }
            ]
        },
        {
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#616161"
                }
            ]
        },
        {
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#f5f5f5"
                }
            ]
        },
        {
            "featureType": "administrative.land_parcel",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#bdbdbd"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#eeeeee"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#e5e5e5"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#9e9e9e"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#dadada"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#616161"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#9e9e9e"
                }
            ]
        },
        {
            "featureType": "transit.line",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#e5e5e5"
                }
            ]
        },
        {
            "featureType": "transit.station",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#eeeeee"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#32383f"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#9e9e9e"
                }
            ]
        }
    ];
var markers = [];

function placeMarker(pos, map, stationName) {

    var marker = new google.maps.Marker({
        position: pos,
        // label: stationName,
        map: map,
        // draggable: true,
        // stationName: stationName,
    });

    markers[stationName] = marker;
}

function removeMarker(stationName) {

    markers[stationName].setMap(null);
    markers[stationName] = null;
}


function initMap() {
    var valuesSet = false;
    var latCenter, lngCenter, latA, latB, lngA, lngB;
    // init values for czechia
    latCenter = 49.8001304;
    lngCenter = 15.3539416;


    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: latCenter, lng: lngCenter},
        zoom: 2,
        styles: style

    });
}

